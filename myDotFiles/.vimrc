source $HOME/.vim/config/plugins.vim
source $HOME/.vim/config/main.vim
source $HOME/.vim/config/plugin-setting.vim
source $HOME/.vim/config/mappings.vim
source $HOME/.vim/config/functions.vim

source $HOME/.vim/config/cocConf.vim
