#=================================================#
#============== Zsh Basic setting ================#
ZSH_THEME="powerlevel10k/powerlevel10k"
export ZSH="$HOME/.oh-my-zsh"
plugins=(git vi-mode docker zsh-autosuggestions zsh-syntax-highlighting zsh-completions)
source $ZSH/oh-my-zsh.sh

export PATH=$PATH:$HOME/.local/bin
export VISUAL=vim

#============== autosuggest accept ===============#
bindkey '^[[Z' autosuggest-accept

#=================== Cursor ======================#

#=================== pfetch ======================#
export PF_COL1=7
export PF_COL3=7
pfetch

#=================== Alias =======================#
alias vi="vim"
alias lg="lazygit"
n ()
{
    # Block nesting of nnn in subshells
    if [ -n $NNNLVL ] && [ "${NNNLVL:-0}" -ge 1 ]; then
        echo "nnn is already running"
        return
    fi

    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    nnn "$@"

    if [ -f "$NNN_TMPFILE" ]; then
            . "$NNN_TMPFILE"
            rm -f "$NNN_TMPFILE" > /dev/null
    fi
}

#===================== p10k ======================#
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
typeset -g POWERLEVEL9K_INSTANT_PROMPT=quiet
#=================================================#
