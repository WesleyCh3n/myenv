#!/bin/sh
set -e

info() {
    echo "[${CYAN}INFO${NC}]" "$@"
}
warn()
{
    echo "[${RED}WARN${NC}]" "$@" >&2
}

setup_color() {
    RED=$(printf '\033[0;31m')
    CYAN=$(printf '\033[0;36m')
    ORANGE=$(printf '\033[0;33m')
    NC=$(printf '\033[0m')
}

setup_env() {
    info "System update"
    sudo apt-get -qq update >/dev/null
    info "Install git & zsh"
    sudo apt-get -qq -y --no-install-recommends install git zsh fontconfig>/dev/null
}

on_my_zsh() {
    ohPATH="${HOME}/.oh-my-zsh/"
    if [ -d $ohPATH ]; then
        info "oh-my-zsh exist. Deleting"
        sudo rm -r $ohPATH
    fi
    info "Install oh-my-zsh"
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
}

git_essential() {
    info "Install zsh-autosuggestions "
    git clone --quiet https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
    info "Install zsh-syntax-highlighting"
    git clone --quiet https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
    info "Install zsh-completions"
    git clone --quiet https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:=~/.oh-my-zsh/custom}/plugins/zsh-completions
    info "Install powerlevel10k"
    git clone --quiet --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
    info "Get my zshrc"
    curl -o $HOME/.zshrc -fsSL https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/.zshrc
}
install_font() {
    info "Get MesloLGS font"
    # curl menslo-regluar
    curl -s --create-dirs -o ${HOME}/.fonts/MesloLGS-NF-Regular.ttf -L https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
    # install font 
    info "Install font"
    fc-cache -f -v >/dev/null
    info "${ORANGE}Please change your terminal font to ${NC}${RED}MesloLGS NF Regular${NC}${ORANGE} after installation complete${NC}"
}
get_pfetch(){
    sudo curl -s -o /usr/bin/pfetch https://raw.githubusercontent.com/dylanaraps/pfetch/master/pfetch
    sudo chmod +x /usr/bin/pfetch
}
{
    setup_color
    setup_env
    on_my_zsh
    git_essential
    install_font
    info "Change zsh to default shell"
    sudo chsh -s /bin/zsh
    info "Installing pfetch"
    get_pfetch
    info "${CYAN}Installation Complete!${NC}"
    info "${ORANGE}Logout and login again. Have a good day!${NC}"
}
