## myZSH Configuration
- Install zsh git (to install plugin)
- [oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh) plugins
    - [zsh-autosuggestions](https://github.com/zsh-users/zsh-autosuggestions)
    - [zsh-syntax-highlighting](https://github.com/zsh-users/zsh-syntax-highlighting)
    - [zsh-completions](https://github.com/zsh-users/zsh-completions)
- [powerlevel10k](https://github.com/romkatv/powerlevel10k)
- my [zshrc](https://gitlab.com/WesleyCh3n/myenv/-/blob/master/myDotFiles/.zshrc) file
    - enable plugins: git vi-mode zsh-autosuggestions zsh-syntax-highlighting zsh-completions
    - cursor mode (Different shape in normal and insert mode with `vi-mode` plugin)
    - `Shift+Tab` autosuggest-accept

#### Installation
```bash
curl -fsSL https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myZshInstall.sh | sh
```

After installation complete, `p10k configuration` will pop up when first login to zsh. Follow the instruction and set your favorite look~

## myVIM Configuration
- Install vim git (to install plugin)
- [vim-plug](https://github.com/junegunn/vim-plug)
- [plugins](https://gitlab.com/WesleyCh3n/myenv/-/blob/master/myDotFiles/plugin.vimrc)
- my [vimrc](https://gitlab.com/WesleyCh3n/myenv/-/blob/master/myDotFiles/.vimrc) file
#### Installation
```bash
curl -fsSL https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myVimInstall.sh | bash -s -
```
For [coc](https://github.com/neoclide/coc.nvim), please install node manually.

And install following plugins by typing `:CocInstall <plugins>`

The Plugins I used: coc-json, coc-explorer, [Optional]coc-python, [Optional]coc-tsserver
#### Update Configuration
```bash
curl -fsSL https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myVimInstall.sh | bash -s - --update
```

## myTMUX Configuration
- Install tmux
- [tmux-plugin-manager](https://github.com/tmux-plugins/tpm)
- plugins:
    - tmux-resurrect
    - tmux-prefix-highlight
- my [tmux.conf](https://gitlab.com/WesleyCh3n/myenv/-/blob/master/myDotFiles/.tmux.conf)
- my [scheme](https://gitlab.com/WesleyCh3n/myenv/-/blob/master/myDotFiles/tmux-gruvbox-dark.conf)
#### Installation
```bash
curl -fsSL https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myTmuxInstall.sh | sh
```
#### Update Configuration
```bash
curl -fsSL https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myTmuxInstall.sh | bash -s - --update
```

