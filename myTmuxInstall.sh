#!/bin/sh
set -e

ARGS_NUM=$#
ARGS=($@)

info() {
    echo "[${CYAN}INFO${NC}]" "$@"
}
warn()
{
    echo "[${RED}WARN${NC}]" "$@" >&2
}

setup_color() {
    RED=$(printf '\033[0;31m')
    CYAN=$(printf '\033[0;36m')
    ORANGE=$(printf '\033[0;33m')
    NC=$(printf '\033[0m')
}
setup_env() {
    info "System update"
    sudo apt-get -qq update >/dev/null
    info "Install tmux & git"
    sudo apt-get -qq -y --no-install-recommends install tmux git >/dev/null
}
tmux_plug() {
    info "Installing Tmux Plugin Manager"
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
    info "Copy config"
    curl -o $HOME/.tmux.conf -fsSL https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/.tmux.conf
    info "Making directory"
    mkdir -p $HOME/.config/tmux/
    info "Copy scheme config"
    curl -o $HOME/.config/tmux/tmux-gruvbox-dark.conf -fsSL https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/tmux-gruvbox-dark.conf
    info "Installing plugins"
    $HOME/.tmux/plugins/tpm/scripts/install_plugins.sh
}
update() {
    info "Update config"
    curl -o $HOME/.tmux.conf -fsSL https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/.tmux.conf
    curl --create-dirs -o $HOME/.config/tmux/tmux-gruvbox-dark.conf -fsSL https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/tmux-gruvbox-dark.conf
    info "Update Complete"
}
{
    setup_color
    if [[ $ARGS_NUM == 0 ]];then
        setup_env
        tmux_plug
        info "${CYAN}Installation Complete!${NC}"
    elif [[ $ARGS_NUM != 0 ]];then
        if [[ ${ARGS[0]} == "--update" ]];then
            update
        fi
    fi
}
