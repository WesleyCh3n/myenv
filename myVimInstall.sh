#!/bin/bash

ARGS_NUM=$#
ARGS=($@)

info() {
    echo "[${CYAN}INFO${NC}]" "$@"
}
warn()
{
    echo "[${RED}WARN${NC}]" "$@" >&2
}
setup_color() {
    RED=$(printf '\033[0;31m')
    CYAN=$(printf '\033[0;36m')
    ORANGE=$(printf '\033[0;33m')
    NC=$(printf '\033[0m')
}

check_path() {
    if [[ $PATH =~ ".local/bin" ]]; then
      info "found \$HOME/.local/bin/ in \$PATH"
    else
      warn "missing \$HOME/.local/bin/ in \$PATH"
      mkdir -p $HOME/.local/
      if [[ $SHELL =~ "bash" ]]; then
        info "bash found"
        SHELLRC=$HOME/.bashrc
      elif [[ $SHELL =~ "zsh" ]]; then
        info "zsh found"
        SHELLRC=$HOME/.zshrc
      else
        echo "neither bash or zsh u r using, please add PATH your own and rerun script"
      fi
      warn \
"Please use following cmd add '.local/bin' to PATH.

  echo \"export PATH=\\\$PATH:\$HOME/.local/bin\" >> $SHELLRC

and source it with

  . $SHELLRC

Then re-run the script again."
      exit 0
    fi
}

debian_setup_env() {
    info "System update"
    sudo apt-get -qq update >/dev/null
    info "Install prerequisite"
    sudo apt-get -qq -y --no-install-recommends install git curl>/dev/null
    info "install node"
    sudo curl -sL install-node.now.sh/lts | sudo bash
}
install_vim() {
    info "Downloading nvim"
    curl -fLo nvim.tar.gz https://github.com/neovim/neovim/releases/download/v0.5.1/nvim-linux64.tar.gz
    info "Extracting"
    tar xf nvim.tar.gz
    mkdir -p $HOME/.local/
    mkdir -p $HOME/.config/nvim/
    cp -r nvim-linux64/* $HOME/.local/
}
vim_plug() {
    info "Installing vim-plug"
    curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    info "Copy plugin config"
    curl -fLo $HOME/.vimrc https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/vim/plugins.vim
    curl -fLo $HOME/.config/nvim/init.vim --create-dirs https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/init.vim
    curl -fLo $HOME/.vim/config/main.vim --create-dirs https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/vim/main.vim
    curl -fLo $HOME/.vim/config/plugins.vim --create-dirs https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/vim/plugins.vim
    curl -fLo $HOME/.vim/config/plugin-setting.vim --create-dirs https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/vim/plugin-setting.vim
    curl -fLo $HOME/.vim/config/mappings.vim --create-dirs https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/vim/mappings.vim
    curl -fLo $HOME/.vim/config/functions.vim --create-dirs https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/vim/functions.vim
    curl -fLo $HOME/.vim/config/cocConf.vim --create-dirs https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/vim/cocConf.vim
}
install_plugins() {
    info "Install plugins"
    sed -i '/wakatim/s/^/"/g'  $HOME/.vim/config/plugins.vim
    nvim +'PlugInstall --sync' +qa
    info "Get my vimrc"
    curl -fLo $HOME/.vimrc https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/.vimrc
}
update() {
    info "Update config"
    curl -o $HOME/.vim/config/main.vim -fsSL --create-dirs https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/vim/main.vim
    curl -o $HOME/.vim/config/plugins.vim -fsSL --create-dirs https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/vim/plugins.vim
    curl -o $HOME/.vim/config/plugin-setting.vim -fsSL --create-dirs https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/vim/plugin-setting.vim
    curl -o $HOME/.vim/config/mappings.vim -fsSL --create-dirs https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/vim/mappings.vim
    curl -o $HOME/.vim/config/functions.vim -fsSL --create-dirs https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/vim/functions.vim
    curl -o $HOME/.vim/config/cocConf.vim -fsSL --create-dirs https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/vim/cocConf.vim
    curl -o $HOME/.vimrc -fsSL https://gitlab.com/WesleyCh3n/myenv/-/raw/master/myDotFiles/.vimrc
    info "Update Complete"
}

{
    setup_color
    check_path
    if [[ $ARGS_NUM == 0 ]];then
        if [ -f "/etc/arch-release" ]; then
            info "System update"
            sudo pacman -Syu
            info "Install prerequisite"
            sudo pacman -S gvim
            sudo curl -sL install-node.now.sh/lts | bash
            vim_plug
            install_plugins
            info "Install COC plugins"
            vim +'CocInstall -sync coc-json coc-explorer' +qall
        else
            debian_setup_env
            install_vim
            vim_plug
            install_plugins
            info "Install COC plugins"
            nvim +'CocInstall -sync coc-json coc-explorer' +qall
            nvim +CocUpdateSync +qall
            echo "{
    \"explorer.icon.enableNerdfont\": true,
    \"explorer.quitOnOpen\": true
}" >> $HOME/.config/nvim/coc-settings.json
            info "${ORANGE}Cleaning workspace${NC}"
            rm -rf ./nvim-linux64 nvim.tar.gz
            info "${CYAN}Installation Complete!${NC}"
        fi
    elif [[ $ARGS_NUM != 0 ]];then
        if [[ ${ARGS[0]} == "--update" ]];then
            update
        fi
    fi
}
